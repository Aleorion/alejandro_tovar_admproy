import java.util.*;
//Looks for an x value that when calculating the t function integral returns a p value.
public class ValueSearcher{
  //Global variables
  public double p;
  public double dof;
  public double error;
  public double x;
  public double num_seg;
  public double d;

  //Constructor
  public ValueSearcher(double p, double dof, double error, double x, double num_seg, double d){
    this.p = p;
    this.dof = dof;
    this.error = error;
    this.x = x;
    this.num_seg = num_seg;
    this.d = d;
  }

  //Retorna la x ideal
  public double idealX(){
    boolean found = false;
    while(!found){
      Simpsons simpsonsCalc = new Simpsons(num_seg, error, dof, x);
      double actualP = simpsonsCalc.simpsons();
      //System.out.println("ACTUAL X: " + x + " ACTUAL P: " +actualP);
      if(Math.abs(actualP-p) <= error){ //we have found the value
        found = true;
      }
      if(actualP > p){ //Modify x
        x -= d;
      }
      else if(actualP < p){ //Modify x
        x += d;
      }
      if((p-actualP) < 0){
        d/=2;
      }
    }

    return x;

  }



}
