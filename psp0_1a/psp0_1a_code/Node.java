class Node
{
	public double val;
	public Node next;
	public int length;

	public Node(double val)
	{
		this.val = val;
		next = null;
		length = 1;
	}

	public void add(Node n)
	{
		next = n;
		length++;
	}

	public double get(int i)
	{
		if(i<0)return 0.0;

		Node current = this;
		while(i-- > 0)
		{
			if(current.next == null)
				return 0.0;
			else
				current = current.next;
		}
		return current.val;
	}		
}