class List
{
	public Node start;
	public Node tail;
	public int length;
	
	public List()
	{
		start = null;
		length = 0;
	}

	public void add(Node n)
	{
		if(start==null)
		{
			start = n;
			tail = n;
		}else
		{
			tail.add(n);
			tail = n;
		}
		length++;
	}

	public void add(double v)
	{
		Node n = new Node(v);
		if(start==null)
		{
			start = n;
			tail = n;
		}else
		{
			tail.add(n);
			tail = n;
		}
		length++;
	}

	public Node get(int i)
	{
		if(i<0)return null;

		Node current = start;
		while(i-- > 0)
		{
			if(current.next == null)
				return null;
			else
				current = current.next;
		}
		return current;
	}

	public String toString()
	{
		String s = "";
		for (int i = 0; i<length; i++)
		{
			s += get(i).val+", ";	
		}
		return s;
	}
}