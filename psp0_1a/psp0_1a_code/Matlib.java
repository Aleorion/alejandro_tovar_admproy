public class Matlib
{
	public static double mean(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += l.get(i).val;
		}
		return sum/l.length;
	}

	public static double stdev(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += (l.get(i).val-mean(l))*(l.get(i).val-mean(l));
		}
		return Math.sqrt(sum/(l.length-1));
	}	
}