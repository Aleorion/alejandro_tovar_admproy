import java.util.*;
import java.io.*;

class DataLoader
{
	public static List loadList()
	{
		Scanner s = new Scanner(System.in);
		return load(s);
	}

	public static List loadList(String source)
	{
		try
		{
			Scanner s = new Scanner(new File(source));
			return load(s);
		}catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}

	private static List load(Scanner s)
	{
		List l = new List();
		while(s.hasNext())
		{
			try{
				
				l.add(s.nextDouble());

			}catch(Exception e)
			{
				System.out.println(e);
			}
		}
		return l;
	}	
}