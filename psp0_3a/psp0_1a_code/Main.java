class Main
{
	public static void main(String[] args)
	{
		List l;
		if(args.length > 0)
		{
			l = DataLoader.loadList(args[0]);
		}else
		{
			l = DataLoader.loadList();
		}
		System.out.println(l);
		System.out.println("Mean of data: "+Matlib.mean(l));
		System.out.println("Stanard deviation: "+Matlib.stdev(l));		
	}	
}
