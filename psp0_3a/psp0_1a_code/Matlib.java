
public class Matlib
{
	public static double mean(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += l.get(i).val;
		}
		return sum/l.length;
	}

	public static double stdev(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += (l.get(i).val-mean(l))*(l.get(i).val-mean(l));
		}
		return Math.sqrt(sum/(l.length-1));
	}

	public static double sum(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += l.get(i).val;
		}
		return sum;
	}

	public static double summult(List x, List y)
	{
		double sum = 0;
		for (int i = 0; i<x.length; i++)
		{
			sum += x.get(i).val * y.get(i).val;
		}
		return sum;
	}

	public static double sumsquared(List l)
	{
		double sum = 0;
		for (int i = 0; i<l.length; i++)
		{
			sum += l.get(i).val * l.get(i).val;
		}
		return sum;
	}

	public static double beta1(List x, List y)
	{
		double gamma, sigma;
		gamma = (summult(x,y)) - (x.length * mean(x) * mean(y));
		sigma = (sumsquared(x) - (x.length * mean(x) * mean(x)));
		return gamma/sigma;
	}

	public static double beta0(List x, List y)
	{
		return mean(y) - (beta1(x,y) * mean(x));
	}

	public static double rxy(List x, List y)
	{
		double gamma, sigma;
		gamma = (x.length * summult(x,y)) - (sum(x) * sum(y));
		sigma = Math.sqrt( ((x.length*sumsquared(x))-((sum(x))*(sum(x))))*((y.length*sumsquared(y))-((sum(y))*(sum(y)))) );
		return gamma/sigma;
	}

	public static double rsquared(List x, List y)
	{
		return rxy(x,y)*rxy(x,y);
	}

}