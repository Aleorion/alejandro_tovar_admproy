
import java.util.*;

/**
 *
 */
public class Mean {

	/**
	 * Default constructor
	 */
	public Mean() {
	}

	/**
	 * @param dataset
	 * @return
	 */
	public double mean(ArrayList<Double> dataset) {
		// TODO implement here
		double mean = 0;
		double sum = 0;
		for(int i = 0; i < dataset.size(); i++){
			sum += dataset.get(i);
		}

		mean = sum / dataset.size();

		return mean;
	}

}
