import java.util.*;
import java.io.IOException;
public class EstimateSize
{
    public static void main(String[] args) throws IOException
    {
        Scanner input = new Scanner(System.in);
        System.out.println("manual capture (1); direct capture (2)");
        int eleccion = input.nextInt();
        System.out.println("number of classes:");
        int clases = input.nextInt();
        ArrayList<Double> averageSizes = new ArrayList<Double>();
        if(eleccion == 1)
        {
            for(int i = 0; i < clases; i++)
            {
                System.out.println("Number of methods " + i + ": ");
                int num = input.nextInt();
                System.out.println("program length " + i + ": ");
                int longitud = input.nextInt();

                averageSizes.add((double)longitud/(double)num);
            }
        }
        else if (eleccion == 2)
        {
            System.out.println("###Auto LOC scan active.");

            for(int i = 0; i < clases; i++)
            {
                FileRead locsCalc = new FileRead();
                int[] aproxLines = locsCalc.methodLines();
                double averageSize = (double)(aproxLines[1] - aproxLines[0]);
                averageSizes.add(averageSize);
            }
        }

        ArrayList<Double> averageSizesLog = logarithmize(averageSizes);

        Mean meanCalc = new Mean();
        double mean = meanCalc.mean(averageSizesLog);

        StandardDeviation stdCalc = new StandardDeviation();
        double std = stdCalc.std(averageSizesLog, mean);
        double variance = Math.pow(std,2);

        double[] logRanges = new double[5];
        logRanges[0] = mean - 2*std;
        logRanges[1] = mean - std;
        logRanges[2] = mean;
        logRanges[3] = mean + std;
        logRanges[4] = mean + 2*std;
        double[] rangesNormal = new double[5];
        for(int i = 0; i < rangesNormal.length; i++)
        {
            rangesNormal[i] = Math.exp(logRanges[i]);
        }

        System.out.println("VSM: " + rangesNormal[0]);
        System.out.println("SM: " + rangesNormal[1]);
        System.out.println("M: " + rangesNormal[2]);
        System.out.println("L: " + rangesNormal[3]);
        System.out.println("VL: " + rangesNormal[4]);

    }


    public static ArrayList<Double> logarithmize(ArrayList<Double> dataNormal)
    {
        ArrayList<Double> dataLog = new ArrayList<Double>();
        for(int i = 0; i < dataNormal.size(); i++)
        {
            dataLog.add(Math.log(dataNormal.get(i)));
        }
        return dataLog;
    }
}
