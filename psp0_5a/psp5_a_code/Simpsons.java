import java.util.*;
//Calcula la distribucion T con Simpsons
public class Simpsons{
  //Variables globales
  double p, num_seg, w, error, dof, x;
  ArrayList<Double> results;
  //

  //Constructor
  public Simpsons(double num_seg, double error, double dof, double x){
    this.num_seg = num_seg;
    this.x = x;
    this.error = error;
    this.dof = dof;
    this.w = this.x/this.num_seg;
    this.results = new ArrayList<Double>();
    this.p = 0;
  }
  //

  //Devuelve el valor de Simpson
  public double simpsons(){
    double simpsAct = ((this.w/3)*F(0)) + summatory(4) + summatory(2) + ((this.w/3)*F(this.x));
    //System.out.println(simpsAct);
    double simpsAnt = 0;
    //Iterate until acceptable error
    while(Math.abs(simpsAct-simpsAnt) > error){
      simpsAnt = simpsAct;
      //Redefine num_seg to make smaller rectangles
      this.num_seg *= 2;
      this.w = this.x/this.num_seg;
      double sum1 = summatory(4);
      double sum2 = summatory(2);
      simpsAct = ((this.w/3)*F(0)) + summatory(4) + summatory(2) + ((this.w/3)*F(this.x));
      //System.out.println(simpsAct);
    }
    return simpsAct;
  }

  //Devuelve una sumatoria dado un intervalo
  public double summatory(double multiplier){
    double res = 0;
    if(multiplier == 4){
      for(int i = 1; i <= num_seg - 1; i= i+2){
        double resParcial = ((this.w/3)*(multiplier * F(i*this.w)));
        res+= resParcial;
        //System.out.println("Sunmatoria mult 4: " +resParcial + " iteracion: " + i + " F(i*w): " + F(i*this.w));
      }
    }
    if(multiplier == 2){
      for(int i = 2; i <= num_seg - 2; i= i+ 2){
        double resParcial = ((this.w/3)*(multiplier * F(i*this.w)));
        res+= resParcial;
        //System.out.println("Sumatoria mult 2: "+resParcial + " iteracion: " + i + " F(i*w): " + F(i*this.w));
      }
    }
    //System.out.println(res);
    return res;
  }

  //Devuelve una F
  public double F(double x){
    GammaFunction gf = new GammaFunction();
    double res1 = 0;
    double res2 = 0;
    double res = 0;
    res1 = gf.gammaFunction((dof+1)/2)/(Math.pow((dof*3.1416),0.5)*gf.gammaFunction(dof/2));
    res2 = Math.pow((1+((x*x)/dof)), -(dof+1)/2);
    return res1*res2;
  }

}
