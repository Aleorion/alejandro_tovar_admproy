import java.util.Scanner;
public class App{
  //Main method to receive my data
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);

    System.out.println("Ingrese num_seg:");
    double num_seg = input.nextDouble();
    System.out.println("Ingrese x:");
    double x = input.nextDouble();
    System.out.println("Ingrese E:");
    double error = input.nextDouble();
    System.out.println("Ingrese dof:");
    double dof = input.nextDouble();
    
    Simpsons simpsonsCalc = new Simpsons(num_seg, error, dof, x);
    GammaFunction gammaFunctionCalc = new GammaFunction();

    System.out.println(simpsonsCalc.simpsons());

  }
}
